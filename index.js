const BASE_URL = "https://6361cc4d7521369cd05e7b3f.mockapi.io";

let idEdited = null;

// Render todo
function fetchAllToDo() {
  // render all todos service
  onLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      renderList(res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log(`err: `, err);
    });
}
// chạy lần đầu khi load trang
fetchAllToDo();

// Remove todo
function removeTodo(id) {
  onLoading();
  axios({
    url: `${BASE_URL}/todos/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      // gọi api res all todos
      offLoading();
      fetchAllToDo();
    })
    .catch(function (err) {
      offLoading();
      console.log(`err: `, err);
    });
}

// Add todo
function addTodo() {
  var data = layThongTinTuForm();
  let newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      offLoading();
      fetchAllToDo();
    })
    .catch(function (err) {
      offLoading();
      console.log(`err: `, err);
    });
}

function editTodo(id) {
  onLoading();
  axios({
    url: `${BASE_URL}/todos/${id}`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      document.getElementById(`name`).value = res.data.name;
      document.getElementById(`desc`).value = res.data.desc;
      idEdited = res.data.id;
    })
    .catch(function (err) {
      turnOffLoading();
      console.log(`err: `, err);
    });
}

function updateTodo() {
  var data = layThongTinTuForm();
  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      fetchAllToDo();
    })
    .catch(function (err) {
      console.log(`err: `, err);
    });
}
